//
//  SingleWord.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 20/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "SingleWord.h"


@implementation SingleWord


- (NSString *) getWordtext
{
	if (_wordtextallocated == true) {
		return _wordtext;
	} else {
		return nil;
	}
}

- (void) setWordtext:(NSString *)wordtext
{
	if (_wordtextallocated == false) {
		_wordtext = [[[NSMutableString alloc] init] retain];
		_wordtextallocated = true;
	}
	
	[_wordtext setString:wordtext];
}

- (NSString *) getTarget
{
	if (_targettextallocated == true) {
		return _target;
	} else {
		return nil;
	}
}

- (bool) getNewline
{
	return _hasnewline;
}

- (void) setTarget:(NSString *)target
{
	if (_targettextallocated == false) {
		_target = [[[NSMutableString alloc] init] retain];
		_targettextallocated = true;
	}
	
	[_target setString:target];
}

- (LCARSColor) getColor
{
	return _wordcolour;
}


- (WordType) getType
{
	return _wordtype;
}

- (FontSize) getSize
{
	return _fontsize;
}

- (CGRect) getWordrect
{
	return _wordrect;
}

- (bool) getLastword // needed for carriage returns at the end of headers
{
	return _lastword;
}

- (int) getReference
{
	return _reference;
}

- (void) setReference:(int)reference
{
	_reference = reference;
}

- (void) setColor:(LCARSColor)color
{
	_wordcolour = color;
}

- (void) setType:(WordType)type
{
	_wordtype = type;
}

- (void) setSize:(FontSize)size
{
	_fontsize = size;
}

- (void) setWordrect:(CGRect)wordrect
{
	_wordrect = wordrect;
}

- (void) setNewline:(bool)hasnewline
{
	_hasnewline = hasnewline;
}

- (void) setLastword:(bool)lastword
{
	_lastword = lastword;
}

@end
