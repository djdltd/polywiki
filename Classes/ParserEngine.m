//
//  ParserEngine.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 29/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "ParserEngine.h"


@implementation ParserEngine

- (void) setDelegate:(id)delegate
{
	_delegateobject = delegate;
}


- (NSString *) rightString:(NSString *)inputstring:(int)length
{
	
	@try {
		return [inputstring substringWithRange:NSMakeRange([inputstring length]-length, length)];
	}
	@catch (NSException * e) {
		NSLog (@"Caught exception on rightString!!!");
		NSLog (@"Input string was: %@, Length was: %i", inputstring, length);
		return inputstring;
	}
	
}

- (NSString *) leftString:(NSString *)inputstring:(int)length
{
	if (length <= [inputstring length]) {
		return [inputstring substringWithRange:NSMakeRange(0, length)];
	} else {
		return inputstring;
	}
}

- (bool) startsWith:(NSString *)contents:(NSString *)startswith
{
	int len = [startswith length];
	
	if ([[self leftString:contents :len] isEqualToString:startswith] == true) {
		return true;
	} else {
		return false;
	}
}

- (NSString *) shortenfromRight:(NSString *)inputstring:(int)length
{
	if ([inputstring length] >= length) {	
		return [inputstring substringWithRange:NSMakeRange(0, [inputstring length]-length)];
	} else {
		return @"";
	}
}

- (bool) containsString:(NSString *)contents :(NSString *)searchingfor
{
	if ([contents rangeOfString:searchingfor].location != NSNotFound) {
		return true;
	} else {
		return false;
	}
}

- (NSString *) filterstring:(NSString *)inputstring:(NSString *)filter
{
	return [inputstring stringByReplacingOccurrencesOfString:filter withString:@""];
}


- (TagType *) analyseTag:(int)parenttype : (NSString *)contents;
{
	TagType *suppliedtag = [[TagType alloc] init];
	NSArray *paramlist;
	bool tagprocessed = false;
	
	if (tagprocessed == false) {
		if ([self startsWith:contents :@"File:"] == true) {
			tagprocessed = true;
			
			if ([self containsString:contents :@"|"] == true) {
				
				paramlist = [contents componentsSeparatedByString:@"|"];
				
				if ([paramlist count] > 0) {
					NSString *firstparam = [paramlist objectAtIndex:0]; // Grab the first item
					[suppliedtag setTarget:firstparam];
				} else {
					[suppliedtag setTarget:contents]; // If no parameters then set the target to the contents supplied
				}
				
			} else {
				
				[suppliedtag setTarget:contents];
				
			}
			
			NSString *filtered = [suppliedtag getTarget];
			
			filtered = [filtered stringByReplacingOccurrencesOfString:@"File:" withString:@""];
			
			[suppliedtag setTarget:filtered];
			[suppliedtag setType:FILETAG];
		}	
	}
	
	if (tagprocessed == false) {
		
		if ([self containsString:contents :@"|"] == true) {
			
			// Decide if this is a link tag by checking out how many params we have separated by |			
			paramlist = [contents componentsSeparatedByString:@"|"];
			
			if ([paramlist count] == 2) {
				
				tagprocessed = true;
				
				NSString *caption = [paramlist objectAtIndex:1];
				NSString *target = [paramlist objectAtIndex:0];
				
				[suppliedtag setTarget:target];
				[suppliedtag setCaption:caption];				
				[suppliedtag setType:WIKILINKTAG];
				
			}
		}
		
	}
	
	
	
	// This is the last thing that must occur in this method
	// basically if all the checks have not come up with anything
	// then we assume this is a link to a target that has the same name and caption
	// and there are no parameters
	if (tagprocessed == false) {
		if ([self containsString:contents :@":"] == false) {		
			tagprocessed = true;
			[suppliedtag setTarget:contents];
			[suppliedtag setCaption:contents];
			[suppliedtag setType:WIKILINKTAG];
		}
	}
	
	//if (paramlist != nil) {
	//	[paramlist release];
	//}
	
	
	return suppliedtag;
}


- (void) notifyToken:(NSString *) contents:(int)identifier {
	
	//NSLog (@"Wanting to notify...");
	if ([contents isEqualToString:@""] == true) {
		return;
	}
	
	NSString *replacedcontent = contents;
	
	
	int r = 0;
	
	for (r=0;r<[_replaceables count];r++) {
		ParserTag *currentrep = [_replaceables objectAtIndex:r];
		replacedcontent = [replacedcontent stringByReplacingOccurrencesOfString:[currentrep getStarttag] withString:[currentrep getEndtag]];
	}
	
	
	if ([_delegateobject respondsToSelector:@selector(foundToken::)]) {
		
		[_delegateobject foundToken:replacedcontent:identifier];
		
	}
}

- (void) notifyFinished
{
	if ([_delegateobject respondsToSelector:@selector(parsingFinished)]) {
		[_delegateobject parsingFinished];
	}
}

- (void) initialiseTaglist
{
	if (_taglistinitialised != true) {
		_taglistinitialised = true;
		_parsingtags = [[[NSMutableArray alloc] init] retain];
		_imaxtaglength = 0;
	}
}

- (void) initialiseReplaceablelist
{
	if (_replaceablelistinitialised != true) {
		_replaceablelistinitialised = true;
		_replaceables = [[[NSMutableArray alloc] init] retain];
	}
}

- (void) initialisePreparseReplaceablelist
{
	if (_preparsereplaceablesinitialised != true) {
		_preparsereplaceablesinitialised = true;
		_preparsereplaceables = [[[NSMutableArray alloc] init] retain];
	}
}

- (void) addReplaceable:(NSString *)texttoreplace:(NSString *)replacewith
{
	[self initialiseReplaceablelist];
	
	ParserTag *reptag = [[ParserTag alloc] initWithTag:texttoreplace :replacewith :1 :false];
	
	[_replaceables addObject:reptag];
}

- (void) addPreparseReplaceable:(NSString *)texttoreplace:(NSString *)replacewith
{
	[self initialisePreparseReplaceablelist];
	
	ParserTag *reptag = [[ParserTag alloc] initWithTag:texttoreplace :replacewith :1 :false];
	
	[_preparsereplaceables addObject:reptag];
}

- (void) addTokentag:(NSString *)startingtag:(NSString *)endingtag:(int)tokenidentifier
{
	[self initialiseTaglist];
	
	if ([startingtag isEqualToString:endingtag] == true) { // This is a symmetrical tag
		
		ParserTag *parsertag = [[ParserTag alloc] initWithTag:startingtag :endingtag :tokenidentifier :true];
		[_parsingtags addObject:parsertag];
		
	} else { // non-symmetric tag
		
		ParserTag *parsertag = [[ParserTag alloc] initWithTag:startingtag :endingtag :tokenidentifier :false];
		[_parsingtags addObject:parsertag];		
		
	}
	
	
	// Update our maximum tag length. We want our max tag length variable to store the maximum length of a tag
	// This will be used later during the parsing.
	
	if ([startingtag length] == [endingtag length]) {
		if ([startingtag length] > _imaxtaglength) {
			_imaxtaglength = [startingtag length];
		}
	} else {
		if ([startingtag length] > [endingtag length]) {
			if ([startingtag length] > _imaxtaglength) {
				_imaxtaglength = [startingtag length];
			}
		} else {
			if ([endingtag length] > _imaxtaglength) {
				_imaxtaglength = [endingtag length];
			}
		}
	}
}

- (void) testParse:(NSString *)contents
{
	
}

- (bool) isTagappropriate:(NSString *)contents :(NSString *)taginquestion :(int)contentlocation
{
	// For now this method is only used on Symmetrical tags
	// Given a tag, and the contents and where the parser is in the contents we have to check
	// that the tag in question is the correct tag in our tag list for the current index. Basically
	// we need to account for tags that look like == and ===, but ensure that a === is not confused
	// with a ==.
	
	// here are our scenarios this function will need to handle
	// Beginning tags:
	// stuff === header
	//        == needs to return false
	//       === needs to return true
	//
	// ending tags:
	// endheader ===
	//           == needs to return false
	//           === needs to return true
	
	// 
	
	
	// First we need to get the part of the string containing the tag in question and test whether we have more
	// than one matching tag in our list
	
	// Just a note - this method doesn't work to well on tags that start right at the beginning of the content
	// we have to be at least a few chars into the content before this will start working nicely.
	
	NSString *section;
	ParserTag *currenttag;
	NSString *startingtag;
	int matchesfound = 0;
	int t = 0;
	int largesttagindex;
	
	int largesttaglength = 0;
	
	if (contentlocation > _imaxtaglength) {
		if (contentlocation < ([contents length] - _imaxtaglength)) {
			// We're within a good range without causing an index out of range problem
			
			section = [contents substringWithRange:NSMakeRange(contentlocation-_imaxtaglength, (_imaxtaglength *2))];
			
			for (t=0;t<[_parsingtags count];t++) {
				
				currenttag = [_parsingtags objectAtIndex:t];
				
				if ([currenttag isSymmetric] == true) {
					
					startingtag = [currenttag getStarttag];
					
					if ([self containsString:section :startingtag] == true) {
						matchesfound++;
						
						// Store the index and length of the largest tag found in the section of content we've been given
						if ([startingtag length] > largesttaglength) {
							largesttaglength = [startingtag length];
							largesttagindex = t;
						}
					}
				}				
			}
			
			
			if (matchesfound == 1) {
				return true;
			}
			
			if (matchesfound > 1) {
				
				
				currenttag = [_parsingtags objectAtIndex:largesttagindex];
				startingtag = [currenttag getStarttag]; // This is the most appropriate tag because it was the largest one found
				
				// Check if what we've been given matches what is appropriate
				if ([startingtag isEqualToString:taginquestion] == true) {
					
					//NSLog (@"Section: %@, TagInQuestion: %@, ContentLocation: %i, Appropriate: YES", section, taginquestion, contentlocation);
					
					return true; // The tag in question is appropriate.
				} else {
					
					//NSLog (@"Section: %@, TagInQuestion: %@, ContentLocation: %i, Appropriate: NO", section, taginquestion, contentlocation);
					
					return false; // The tag in question is not appropriate. Another tag exists which is more appropriate.
				}
				
			}
		}
	}
	
	//NSLog (@"Section: %@, TagInQuestion: %@, ContentLocation: %i, Appropriate: YES DEFAULT", section, taginquestion, contentlocation);
	
	return true; // Default - if we get here then return a default of true - tag is appropriate.
}

- (void) parse:(NSString *)fullcontents
{
	if (_taglistinitialised != true) { // We can't parse without any tags being added.
		NSLog (@"Parse was called before adding any parser tags!");
		return;
	}
	
	int rp = 0;
	
	NSString *contents = fullcontents;
	
	for (rp=0;rp<[_preparsereplaceables count];rp++) {
		ParserTag *currentrep = [_preparsereplaceables objectAtIndex:rp];
		contents = [contents stringByReplacingOccurrencesOfString:[currentrep getStarttag] withString:[currentrep getEndtag]];
	}
	
	int c = 0;
	int t = 0;
	NSMutableString *builtstring = [[NSMutableString alloc] init];
	
	NSMutableString *plaintext = [[NSMutableString alloc] init]; // this is a token without any tags - basically plain text
	NSMutableString *commandtext = [[NSMutableString alloc] init]; // this is a current tagged token
	
	int tagcount = 0;
	int limit;
	bool singletagmode = false;
	//bool ignorecount = false;
	int lasttagpos = 0;
	
	limit = [contents length];
	
	/*
	if (limit > 6000) {
		limit = 6000;
	}
	*/
	
	if ([contents length] > 1) {
		
		for (c=0;c<limit;c++) {
			
			char currentchar = [contents characterAtIndex:c];
			
			
			for (t=0;t<[_parsingtags count];t++) {
				
				ParserTag *currenttag = [_parsingtags objectAtIndex:t];
				
				NSString *starttag = [currenttag getStarttag];
				NSString *endtag = [currenttag getEndtag];
				int tokenidentifier = [currenttag getIdentifier];
				
				if ([currenttag isSymmetric] == false) { // Regular {{ }} tags
					
					if ([builtstring length] >=[starttag length])
					{
						// Found the starting tag
						if ([[self rightString:builtstring :[starttag length]] isEqualToString:starttag] == true) {
							
							if (c-lasttagpos > ([starttag length]-1)) {
								
								lasttagpos = c;
							
								if (tagcount == 0) {
									[plaintext setString:[self shortenfromRight:plaintext :[starttag length]]];
								}
								tagcount++;
								//NSLog (@"Increased tag count to: %i", tagcount);
							}
						}
					}
					
					
					if ([builtstring length] >=[endtag length]) {
						// Found the ending tag
						if ([[self rightString:builtstring :[endtag length]] isEqualToString:endtag] == true) {
							
							if ((c-lasttagpos) > ([endtag length]-1)) {
							
							//
							
							lasttagpos = c;
							
							
							//if (ignorecount == false) {
								tagcount--;
								//NSLog (@"\n*************************************************START TOKEN ID %i ***********************************************\n%@\n*************************************************END TOKEN ID %i ***********************************************\n", identifier, tokencontents, identifier);NSLog (@"Decreased tag count to: %i", tagcount);
								
								//NSLog (@"Decreased tag count to: %i", tagcount);
								//ignorecount = true;
								if (tagcount == 0) {
									//NSLog (@"Plain object: %@", plaintext);
									
									[self notifyToken:plaintext:0];
									
									[plaintext setString:@""];
									
									[commandtext setString:[self shortenfromRight:commandtext :[endtag length]]];
									
									//NSLog (@"Command object: %@", commandtext);
									[self notifyToken:commandtext:tokenidentifier];
									
									[commandtext setString:@""];
								}
							//}
							}
						} else {
							//ignorecount = false;
						}
					}
					
				} else { // Symmetric e.g. === ===
					
					if ([builtstring length] >=[starttag length])
					{
						// Found a symmetric tag
						if ([[self rightString:builtstring :[starttag length]] isEqualToString:starttag] == true) {
							
							if ([self isTagappropriate:contents :starttag :c] == true) {
							
								if (singletagmode == false) {
									if (c-lasttagpos > ([starttag length]-1)) {
										lasttagpos = c;
										
										singletagmode = true;
										if (tagcount == 0) {
											[plaintext setString:[self shortenfromRight:plaintext :[starttag length]]];
										}
										tagcount++;
									}
								} else {
									if ((c-lasttagpos) > ([starttag length]-1)) {
										lasttagpos = c;
										
										singletagmode = false;
										tagcount--;
										
										if (tagcount == 0) {
											//NSLog (@"Plain object: %@", plaintext);
											
											[self notifyToken:plaintext:0];
											
											[plaintext setString:@""];
											
											[commandtext setString:[self shortenfromRight:commandtext :[starttag length]]];
											
											//NSLog (@"Command object: %@", commandtext);
											[self notifyToken:commandtext:tokenidentifier];
											
											[commandtext setString:@""];
										}	
									}
								}
								
							} // Is tag appropriate	
						}
					}
					
				} // isSymmetric tag
				
			} // for loop - tag enumeration
			
			/*
			if ([builtstring length] >=2)
			{
				// Found the starting tag
				if ([[self rightString:builtstring :2] isEqualToString:@"{{"] == true) {
					
					if (tagcount == 0) {
						[plaintext setString:[self shortenfromRight:plaintext :2]];
					}
					tagcount++;
				}
				
				// Found the ending tag
				if ([[self rightString:builtstring :2] isEqualToString:@"}}"] == true) {
					
					tagcount--;
					
					if (tagcount == 0) {
						//NSLog (@"Plain object: %@", plaintext);
						
						[self notifyToken:plaintext];
						
						[plaintext setString:@""];
						
						[commandtext setString:[self shortenfromRight:commandtext :2]];
						
						//NSLog (@"Command object: %@", commandtext);
						[self notifyToken:commandtext];
						
						[commandtext setString:@""];
					}
				}
			}
			
			
			
			if ([builtstring length] >=3)
			{
				// Found a symmetric tag
				if ([[self rightString:builtstring :3] isEqualToString:@"==="] == true) {
					
					if (singletagmode == false) {
						singletagmode = true;
						if (tagcount == 0) {
							[plaintext setString:[self shortenfromRight:plaintext :3]];
						}
						tagcount++;
					} else {
						
						singletagmode = false;
						tagcount--;
						
						if (tagcount == 0) {
							//NSLog (@"Plain object: %@", plaintext);
							
							[self notifyToken:plaintext];
							
							[plaintext setString:@""];
							
							[commandtext setString:[self shortenfromRight:commandtext :3]];
							
							//NSLog (@"Command object: %@", commandtext);
							[self notifyToken:commandtext];
							
							[commandtext setString:@""];
						}	
						
					}
				}
			}
			*/
			
			
			[builtstring appendString:[NSString stringWithFormat:@"%c", currentchar]];
			
			
			if (tagcount >= 1) {
				[commandtext appendString:[NSString stringWithFormat:@"%c", currentchar]];
			}
			
			if (tagcount == 0) {
				[plaintext appendString:[NSString stringWithFormat:@"%c", currentchar]];
			}
		}
		
		
		//NSLog (@"Plain text: %@", plaintext);
		
		
		[self notifyToken:plaintext:0];
		
		
		for (t=0;t<[_parsingtags count];t++) {
			
			ParserTag *currenttag = [_parsingtags objectAtIndex:t];
			
			NSString *starttag = [currenttag getStarttag];
			NSString *endtag = [currenttag getEndtag];
			int tokenidentifier = [currenttag getIdentifier];
			
			if ([commandtext length] > [endtag length]) {
				if ([[self rightString:commandtext :[starttag length]] isEqualToString:starttag] == true) {
					[commandtext setString:[self shortenfromRight:commandtext :[starttag length]]];
					[self notifyToken:commandtext:tokenidentifier];
				} else {
					if ([[self rightString:commandtext :[endtag length]] isEqualToString:endtag] == true) {
						[commandtext setString:[self shortenfromRight:commandtext :[endtag length]]];
						[self notifyToken:commandtext:tokenidentifier];
					}
				}
				
				
			}
			
			/*
			if ([commandtext length] > 3) {
				if ([[self rightString:commandtext :3] isEqualToString:@"==="] == true) {
					[commandtext setString:[self shortenfromRight:commandtext :3]];
					[self notifyToken:commandtext];
				}
			}
			*/
		}
		
		

		
		//NSLog (@"Command text: %@", commandtext);
		[self notifyFinished];

	}
}

@end
