//
//  WikiPart.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 18/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "WikiPart.h"


@implementation WikiPart


// Get methods
- (bool) hasNewline
{
	return _newline;
}

- (int) getParttype
{
	return _parttype;
}

- (bool) isHeading
{
	return _heading;
}

- (NSString *) getContents
{
	return _contents;
}

- (NSString *) getCaption
{
	return _caption;
}

- (NSString *) getTarget
{
	return _target;
}

// Set methods
- (void) setNewline:(bool)hasnewline
{
	_newline = hasnewline;
}

- (void) setParttype:(int)parttype
{
	_parttype = parttype;
}

- (void) setHeading:(bool)isheading
{
	_heading = isheading;
}

- (void) setContents:(NSString *)contents
{
	if (_contentsinitialised == false) {
		_contentsinitialised = true;
		_contents = [[[NSMutableString alloc] init] retain];
	}
	
	[_contents setString:contents];
}

- (void) setCaption:(NSString *)caption
{
	if (_captioninitialised == false) {
		_captioninitialised = true;
		_caption = [[[NSMutableString alloc] init] retain];
	}
	
	[_caption setString:caption];
}

- (void) setTarget:(NSString *)target
{
	if (_targetinitialised == false) {
		_targetinitialised = true;
		_target = [[[NSMutableString alloc] init] retain];
	}
	
	[_target setString:target];
}

@end
