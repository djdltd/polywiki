//
//  LCARSLabelView.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 26/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmallAlpha.h"

@interface LCARSLabelView : UIView {

	SmallAlpha *_lcarsstring;
	
	NSMutableString *_captionstring;
	
}

- (NSString *) shortenfromRight:(NSString *)inputstring:(int)length;
- (void)setCaption:(NSString *)caption;
- (NSString *)getCaption;
- (void)appendCaption:(NSString *)textvalue;
- (void)backspaceCaption;

@end
