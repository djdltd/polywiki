//
//  SingleWord.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 20/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {

	Blue,
	Orange,
	Red,
	Yellow

} LCARSColor;

typedef enum {

	Text,
	LineBreak,
	WikiLink,
	PlainText

} WordType;

typedef enum {

	Small,
	Medium,
	Large
	
} FontSize;

@interface SingleWord : NSObject {
	
	
	LCARSColor _wordcolour;
	WordType _wordtype;
	FontSize _fontsize;
	CGRect _wordrect;
	
	bool _hasnewline;
	bool _wordtextallocated;
	bool _targettextallocated;
	bool _lastword;
	int _reference;
	
	NSMutableString *_wordtext;
	NSMutableString *_target;
}

- (NSString *) getWordtext;
- (void) setWordtext:(NSString *)wordtext;

- (NSString *) getTarget;
- (void) setTarget:(NSString *)target;

- (LCARSColor) getColor;
- (WordType) getType;
- (FontSize) getSize;
- (CGRect) getWordrect;
- (bool) getNewline;
- (bool) getLastword; // needed for carriage returns at the end of headers
- (int) getReference;

- (void) setReference:(int)reference;
- (void) setNewline:(bool)hasnewline;
- (void) setLastword:(bool)lastword;
- (void) setColor:(LCARSColor)color;
- (void) setType:(WordType)type;
- (void) setSize:(FontSize)size;
- (void) setWordrect:(CGRect)wordrect;

@end
