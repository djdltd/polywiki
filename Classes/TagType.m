//
//  TagType.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 12/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "TagType.h"


@implementation TagType

- (TagType *) initWithTag:(NSString *)caption :(NSString *)target :(int)type
{
	self = [super init];
	
	if (self) {
		
		_caption = [[[NSMutableString alloc] initWithString:caption] retain];
		_target = [[[NSMutableString alloc] initWithString:target] retain];
		_tagtype = type;
		
		_targetinitialised = true;
		_captioninitialised = true;
		
		return self;
	} else {
		return nil;
	}
}

- (int) getType
{
	return _tagtype;
}

- (NSString *) getCaption
{
	return _caption;
}

- (NSString *) getTarget
{
	return _target;
}

- (void) setType:(int)type
{
	_tagtype = type;
}

- (void) setCaption:(NSString *)caption
{
	if (_captioninitialised != true) {
		
		_caption = [[[NSMutableString alloc] initWithString:caption] retain];
		_captioninitialised = true;
	} else {
		[_caption setString:caption];
	}
}

- (void) setTarget:(NSString *)target
{
	if (_targetinitialised != true) {
		_target = [[[NSMutableString alloc] initWithString:target] retain];
		_targetinitialised = true;
	} else {
		[_target setString:target];
	}
}

@end
