//
//  LCARS_WikipediaAppDelegate.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 12/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LCARS_WikipediaViewController;

@interface LCARS_WikipediaAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    LCARS_WikipediaViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet LCARS_WikipediaViewController *viewController;

@end

